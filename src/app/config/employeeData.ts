export const EmployeeData = [
    {
        name:"Ravi Kumar",
        employeeId:"123",
        department:'Data Science',
        emailId:"a@g.com",
        doj:"12-02-1994"
    },
    {
        name:"Sonali Kumar",
        employeeId:"123",
        department:'Backend',
        emailId:"a@g.com",
        doj:"12-02-1994"
    },
    {
        name:"Kiran Kumar",
        employeeId:"123",
        department:'Frontend',
        emailId:"a@g.com",
        doj:"12-02-1994"
    }
]
