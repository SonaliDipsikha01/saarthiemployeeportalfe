import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { EmployeeFormComponent } from '../employee-form/employee-form.component'
import { EmployeeData } from '../../config/employeeData';

interface EmployeeData{
  name:string,
  employeeId:string,
  department:string,
  emailId:string,
  doj:string
}
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  dataSource = new MatTableDataSource([]); // Used for storing the table display data

  public headerConfig: Array<string> = ['Action', 'Name', 'Employee ID', 'Department', 'Email ID', 'DOJ']
  public filterFormGroup: FormGroup;
  public allData:Array<EmployeeData>= [];
  constructor(public dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) { }

  ngOnInit() {
    this.allData = this.getFromLocalStorage() || [];
    //added some dummy data to be showed in the table
    if (this.allData.length == 0) {
      this.allData = (JSON.parse(JSON.stringify(EmployeeData))); 
      this.setToLocalStorage(this.allData)//on page refresh, it will allways retain the update data
    }
    this.dataSource.data = this.allData;
  }

/**
   * addEmployee
   * @description It handles dialog open and close 
   */
  addEmployee(): void {
    const dialogRef = this.dialog.open(EmployeeFormComponent, {
      width: '400px',
      data: {},
      panelClass: 'ui-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updateEmployeeList(result);
      }
    });
  }

  /**
   * updateEmployeeList
   * @description update the ui with new data as well as update local storage to retain data
   */
  private updateEmployeeList(result: any):void{
    this.allData.push(result);
    this.setToLocalStorage(this.allData)
    this.dataSource.data = [...this.allData];
    this.changeDetectorRefs.detectChanges();
  }

  /**
   * setToLocalStorage
   * @description used to update data in local storage
   */
  setToLocalStorage(data) {
    localStorage.setItem('employeeData', JSON.stringify(data))
  }

   /**
   * getFromLocalStorage
   * @description get update data on page refresh 
   */
  getFromLocalStorage() {
    return JSON.parse(localStorage.getItem('employeeData'))
  }

  /**
   * deleteRow
   * @description delete row and update local storage
   */
  deleteRow(index:number) {
    this.allData.splice(index, 1);
    this.dataSource.data = [...this.allData];
    this.setToLocalStorage(this.allData)
    this.changeDetectorRefs.detectChanges();
  }
}
