import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import moment from 'moment';
@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  public departments= ["Frontend", "Backend", "Data Science", "Infra Team", "HR"]
  public employeeFormGroup: FormGroup;
  public regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,7}))$/;
  public fieldData: any = {};
  constructor(private _formBuilder: FormBuilder, public dialogRef: MatDialogRef<EmployeeFormComponent>) { }

  ngOnInit() {
    this.initiateForm();
  }

   /**
   * @description intiate form intiate form group with controller and validation
   */
  initiateForm():void {
    this.employeeFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      employeeId: ['', Validators.required],
      department: [''],
      emailId: ['', [Validators.required, Validators.pattern(this.regexEmail)]],
      doj: ['', Validators.required]
    })
  }
  
  /**
   * formSubmit
   * @description on form submit pass form data to parent employee details compoonent
   */
  formSubmit():void {
    this.employeeFormGroup.value.doj = moment(this.employeeFormGroup.value.doj).format("YYYY-MM-DD"); //changes date format
    this.dialogRef.close(this.employeeFormGroup.value)
  }
 
  /**
   * clearForm
   * @description reset form
   */
  clearForm() {
    this.employeeFormGroup.reset()
  }
}
