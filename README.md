# README #
SaarthEmploeePortal

### Required in system ###
1.Node js
2.npm package manager

### How do I get set up? ###
1.clone the repository.
2.move inside the folder(Master Branch) 
3.install package.json using 'npm i', so all the dependecy will be available to run the projetc
4.'npm i' will create node modules folder with all the dependecies

### How do I run the project? ###
Once project set up done with above steps,
run 'ng serve' to build and to launche the server.

Go to 'http://localhost:4200/'

Or can also run the build main.ts file inside dist folder (for eg with python)

### Responsibilties  ###

1. Create a Button -NewEmployee
2. On click of NewEmployee Button a FORM should popup asking below input to be filled:
a) Name- Input [required]
b) Employee ID- Input.[required]
c) Department- dropdown 
d) Email Id-text [required and valid]
e) Date of Joining(DOJ)- add calendar to selectdate.[required]
f) Submit Details- a button to submit data.
g) Clear- a button to clear formdata.
3. On click ofSubmit Detailsbutton,FORM should get closed and adynamic table gets created on
a) Form should not get submitted with empty data.
b) Empty entry error message should be thrown.
4. On click of delete icon,row gets deleted.